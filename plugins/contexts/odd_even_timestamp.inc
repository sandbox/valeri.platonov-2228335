<?php

/** Context plugin data:
    'title' => Visible title
    'description' => Description of context
    'context' => Callback to create a context. Params: $empty, $data = NULL, $conf = FALSE
    'settings form' => Callback to show a context setting form. Params: ($conf, $external = FALSE)
    'settings form validate' => params: ($form, &$form_values, &$form_state)
    'settings form submit' => params: 'ctools_context_adyax_test_settings_form_submit',
    'keyword' => The default keyword to use.
    'context name' => The unique identifier for this context for use by required context checks.
    'no ui' => if TRUE this context cannot be selected.
*/

$plugin = array(
  'title' => t('adyax test odd even time'),
  'description' => t('adyax test odd even time'),
  'context' => 'adyax_test_context_value',
  'keyword' => 'adyax_test_odd_even_time',
  'context name' => 'AdyaxTestOddEvenTime',
  'no ui' => FALSE,
);

/**
  ctools_context::__set_state(array(
   'type' => 'any',
   'data' => NULL,
   'title' => 'No context',
   'page_title' => '',
   'identifier' => 'No context',
   'argument' => NULL,
   'keyword' => '',
   'original_argument' => NULL,
   'restrictions' => array(),
   'empty' => false,
))
 * @param unknown $empty
 * @param string $data
 * @param string $conf
 * @return stdClass
 */
function adyax_test_context_value($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('AdyaxTestOddEvenTime');
  $context->type = 'AdyaxTestOddEvenTime';
  $context->data = _dummy_unix_timestamp();
  $context->title = t('adyax test odd even time');
  return $context;
}
