<?php
/**
 *   // Название плагина, которое будет отображено при его выборе.
  'title' => t('Simplecontext content type'),
  // Описание плагина, которое будет отображено при его выборе.
  'description' => t('Simplecontext content type - works with a simplecontext context.'),
  // Конструктор.
  'content_types' => 'simplecontext_content_type',
  // Показывает, является ли плагин подтипом другого. В данном случае - не является.
  'single' => TRUE,
  // Название функции, которая будет создавать блок с данными.
  'render callback' => 'simplecontext_content_type_render',
  // Иконка, отображаемая при выборе плагина.
  'icon' => 'icon_example.png',
  // Контекст, который необходимо передать плагину для его работы.
  'required context' => new ctools_context_required(t('Simplecontext'), 'simplecontext'),
  // Форма с дополнительными настройками плагина.
  'edit form' => 'simplecontext_content_type_edit_form',
  // Административный заголовок модуля, который может быть изменён через веб интерфейс.
  'admin title' => 'ctools_plugin_example_simplecontext_content_type_admin_title',
  // Создаёт блок предпросмотра результата в Panels.
  'admin info' => 'ctools_plugin_example_simplecontext_content_type_admin_info',
  // Категория, в которую будет помещён плагин, и его вес.
  'category' => array(t('CTools Examples'), -9),
  // Контексты, передаваемые по умолчанию.
  'defaults' => array(),
 */
/**
 * @file
 * Unix timestamp time() so that if timestamp is odd it returns TRUE, and FALSE if even
 */

$plugin = array(
  'title' => t('Adyax test Odd Even Content'),
  'single' => TRUE,
  'description' => t('Adyax test Odd Even Content'),
  'render callback' => 'adyax_test_odd_even_timestamp_content_type_render',
  'required context' => new ctools_context_required(t('Adyax test'), 'AdyaxTestOddEvenTime'),
  'category' => array(t('Adyax Text'),-5 ),
  'defaults' => array(),
);

/**
 * Render pane content.
 */
function adyax_test_odd_even_timestamp_content_type_render($subtype, $conf, $panel_args, $context) {
  $context_data = $context->data ? 'TRUE' : 'FALSE';
  $block = new stdClass();
  $block->content .= '<div class="odd-even-time"><h1>' . 'Odd Even TimeStamp' . '</h1><div class="odd-even-time-value">' . $context_data . '</div></div>';
  return $block;
}
