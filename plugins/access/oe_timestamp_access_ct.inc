<?php

/**
    'title' => Title of the plugin
    'description' => Description of the plugin
    'callback' => callback to see if there is access is available. params: $conf, $contexts, $account
    'required context' => new ctools_context_required(t('Node'), 'node'), // zero or more required contexts for this access plugin
    'default' => an array of defaults or a callback giving defaults
    'settings form' => settings form. params: &$form, &$form_state, $conf
     settings form validate
     settings form submit
*/

$plugin = array(
  'title' => t('adyax test odd even time access'),
  'description' => t('adyax test odd even time access'),
  'callback' => 'adyax_test_context_ctools_access_check',
  'required context' => new ctools_context_required(t('Adyax test'), 'AdyaxTestOddEvenTime'),
  'default' => array('negate' => 0),
  'settings form' => 'adyax_test_odd_even_time_type_ctools_access_settings',
  'summary' => 'adyax_test_odd_even_time_type_ctools_access_summary',
);


/**
 * Check for access.
 */
function adyax_test_context_ctools_access_check($conf, $contexts) {
  if($contexts && $contexts->type == 'AdyaxTestOddEvenTime') return $contexts->data;
  return TRUE;
}

/**
 * Settings form for the 'by parent term' access plugin.
 */
function adyax_test_odd_even_time_type_ctools_access_settings($form, &$form_state, $conf) {
  // No additional configuration necessary.
  return $form;
}

/**
 * Provide a summary description based upon the checked context.
 */
function adyax_test_odd_even_time_type_ctools_access_summary($conf, $context) {
  return t('Current time is your passing');
}


