SOME "Adyax SAS" TEST for 6-7 Drupal
take from Soft-Group Kiev UA

Create a CTools context plugin that returns TRUE/FALSE based on current Unix timestamp (time() function) 
so that if timestamp is odd it returns TRUE, and FALSE if even. Create CTools Access plugin that require 
created above context and allow access if context is TRUE. Create a CTools Content Type, that require 
context from above, and shows its value in text form ‘TRUE’ / ‘FALSE’.

Organize your code as module named ‘adyax_test’. Add a feature (using Feature module) that if enabled will 
provide a page (panels page) with address /test3 that demonstrate task results - CCT from above is placed 
twice on a page, first time it is placed as is, and second time using Access plugin from above, So that 
first block will always be visible and shows ‘TRUE’ or ‘FALSE’ and second one will only be visible if 
Context returns TRUE. (No special theming required).

You should write your code respecting drupal standards and approaches.

